import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Router } from '@angular/router';

@Injectable()
export class AuthService {

  private registerUrl = "http://localhost:3000/api/register";
  private loginUrl = "http://localhost:3000/api/login";
  private resetUrl = "http://localhost:3000/api/reset-password";

  constructor(private http: Http,
              private router: Router) { }

  registerUser(user) {
    return this.http.post(this.registerUrl, user)
  }

  loginUser(user) {
    return this.http.post(this.loginUrl, user)
  }

  resetPassword(user) {
    return this.http.post(this.resetUrl, user);
  }

  logoutUser() {
    localStorage.removeItem('token')
    this.router.navigate(['/login'])
  }

  getToken() {
    return localStorage.getItem('token')
  }

  loggedIn() {
    return !!localStorage.getItem('token')
  }
}
