import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

@Injectable()
export class FeedsService {

  private feedsUrl = "http://localhost:3000/api/feeds";

  constructor(private http: Http) { }

  getFeeds() {
    return this.http.get(this.feedsUrl);
  }
}
