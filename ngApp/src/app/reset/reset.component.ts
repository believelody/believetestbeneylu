import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-reset',
  templateUrl: './reset.component.html',
  styleUrls: ['./reset.component.css']
})
export class ResetComponent implements OnInit {

  constructor(private auth: AuthService, private router: Router) { }

  private loginUserData = {};

  ngOnInit() {
  }

  resetPassword() {
    this.auth.resetPassword(this.loginUserData).subscribe(
      () => {
        this.router.navigate(['/login']);
      },
      err => console.log(err)
    )
  }

}
